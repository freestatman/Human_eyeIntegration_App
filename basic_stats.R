library(RSQLite)
library(DBI)
library(pool)

# pools for sqlite DBs
gene_pool <- dbPool(drv = RSQLite::SQLite(), dbname = "./www/eyeIntegration_human_v072.sqlite", idleTimeout = 3600000)
SC_pool <- dbPool(drv = RSQLite::SQLite(), dbname = "./www/single_cell_retina_info_3.sqlite", idleTimeout = 3600000)

basic_stats <- rbind(c(format(dbGetQuery(gene_pool, "SELECT COUNT(*) FROM lsTPM"), big.mark = ','), "Gene expression values"),
                     c(format(dbGetQuery(gene_pool, "SELECT COUNT(*) FROM lsTPM_TX"), big.mark = ','), "Transcript expression values"),
                     c(format(dbGetQuery(gene_pool, "SELECT COUNT(*) FROM edges_rpe"), big.mark = ','), "RPE/choroid network edges"),
                     c(format(dbGetQuery(gene_pool, "SELECT COUNT(*) FROM edges_retina"), big.mark = ','), "Retina network edges"),
                     c(format(dbGetQuery(SC_pool, "SELECT COUNT(*) FROM macosko__SC_gene_counts") + dbGetQuery(SC_pool, "SELECT COUNT(*) FROM clark__SC_gene_counts"), big.mark = ','), "Single cell (mouse) gene expression values")) %>% 
  data.frame()
colnames(basic_stats) <- c('Counts', 'Data Type')
save(basic_stats, file = 'www/basic_stats.Rdata')

