| ID | Species | Tissue | Time Point(s) | Publication | Data | Platform | eyeIntegrated |
| - | - | - | - | - | - | - | - |
| Macosko | Mouse | Retina | P14 | [PubMed](https://www.ncbi.nlm.nih.gov/pubmed/26000488) | [GSE63473](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE63473) | DropSeq | Y |
| Clark | Mouse | Retina | E11, E12, E14, E16, E18, P0, P2, P5, P8, P14 | [bioRxiv](http://biorxiv.org/content/early/2018/07/30/378950) | [Github](https://github.com/gofflab/developing_mouse_retina_scRNASeq) | 10X | Y |
| Lukowski | Human | Retina | Post-mortem | [bioRxiv](https://www.biorxiv.org/content/early/2018/09/24/425223) | NA | 10X | N |
| Lu | Human | Retina | 12-27 weeks | [bioRxiv](https://www.biorxiv.org/content/early/2018/09/23/423830) | [NA](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE116106) | 10X | N | 
| Phillips | Human | hiPSC, Retina | 70,218 Days (hiPSC), Post-mortem  | [PubMed](https://www.ncbi.nlm.nih.gov/pubmed/29230913) | [GEO](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE98556) | Fluidigm | N |
